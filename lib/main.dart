import 'package:flutter/material.dart';
import 'package:sweetapp/welcome_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Sweet App',
      theme: ThemeData(
        primaryIconTheme: IconThemeData(
          color: Color(0xFF4F4F4F)
        ),
        primarySwatch: Colors.blue,
      ),
      home: WelcomeScreen(),
    );
  }
}


